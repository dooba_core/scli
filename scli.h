/* Dooba SDK
 * USB Serial Command Line Interface
 */

#ifndef	__SCLI_H
#define	__SCLI_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <scom/term.h>

// Default Terminal Prompt
#define	SCLI_DEFAULT_PROMPT						""

// Prompt Shortcut
#define	scli_prompt()							scom_term_printf("%t", scli_prompt_s, scli_prompt_s_len)

// Clear Size
#define	SCLI_CLEAR_SIZE							64

// Print Shortcut
#define	scli_printf(fmt, ...)					scom_term_printf(fmt, ##__VA_ARGS__)

// External Command Handler Type
typedef uint8_t (*scli_ext_cmd_handler_t)(void *user, char *full_str, uint16_t full_len, char *cmd, uint16_t cmd_len, char *args, uint16_t args_len);

// Command Structure
struct scli_cmd
{
	// Name
	char *cmd;

	// Handler
	void (*handler)(char **args, uint16_t *args_len);

	// Linked List
	struct scli_cmd *next;
};

// Prompt
extern char *scli_prompt_s;
extern uint8_t scli_prompt_s_len;

// Initialize
extern void scli_init();

// Initialize with Hello Message
extern void scli_init_m(char *hello_fmt, ...);

// Set Prompt
extern void scli_set_prompt(char *prompt);

// Set Prompt (fixed length)
extern void scli_set_prompt_n(char *prompt, uint8_t len);

// Update
extern void scli_update();

// Define Command
extern void scli_def_cmd(struct scli_cmd *c, char *cmd, void (*handler)(char **args, uint16_t *args_len));

// Set External Command Handler
extern void scli_set_ext_cmd_handler(scli_ext_cmd_handler_t handler, void *user);

// List Commands
extern void scli_help();

// Clear Screen
extern void scli_clear();

// Handle Serial Communication
extern uint8_t scli_term_handler(char *x, uint8_t s);

#endif
