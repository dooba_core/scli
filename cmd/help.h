/* Dooba SDK
 * USB Serial Command Line Interface
 */

#ifndef	__SCLI_CMD_HELP_H
#define	__SCLI_CMD_HELP_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "scli.h"

// Help Command
#define	SCLI_CMD_HELP						"help"

// Command Storage
extern struct scli_cmd scli_cmd_help_s;

// Help
extern void scli_cmd_help(char **args, uint16_t *args_len);

#endif
