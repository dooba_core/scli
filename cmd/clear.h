/* Dooba SDK
 * USB Serial Command Line Interface
 */

#ifndef	__SCLI_CMD_CLEAR_H
#define	__SCLI_CMD_CLEAR_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "scli.h"

// Clear Command
#define	SCLI_CMD_CLEAR						"clear"

// Command Storage
extern struct scli_cmd scli_cmd_clear_s;

// Clear
extern void scli_cmd_clear(char **args, uint16_t *args_len);

#endif
