/* Dooba SDK
 * USB Serial Command Line Interface
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "clear.h"

// Command Storage
struct scli_cmd scli_cmd_clear_s;

// Clear
void scli_cmd_clear(char **args, uint16_t *args_len)
{
	// Clear screen
	scli_clear();
}
