/* Dooba SDK
 * USB Serial Command Line Interface
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "help.h"

// Command Storage
struct scli_cmd scli_cmd_help_s;

// Help
void scli_cmd_help(char **args, uint16_t *args_len)
{
	// Display Help
	scli_help();
}
