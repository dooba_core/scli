/* Dooba SDK
 * USB Serial Command Line Interface
 */

// External Includes
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "cmd/help.h"
#include "cmd/clear.h"
#include "scli.h"

// Prompt
char *scli_prompt_s;
uint8_t scli_prompt_s_len;

// Commands
struct scli_cmd *scli_cmd_list;

// External Handler
scli_ext_cmd_handler_t scli_ext_cmd_handler;
void *scli_ext_cmd_handler_user;

// Initialize
void scli_init()
{
	// Set Default Prompt
	scli_prompt_s = SCLI_DEFAULT_PROMPT;
	scli_prompt_s_len = strlen(scli_prompt_s);

	// Clear Command List
	scli_cmd_list = 0;

	// Clear External Handler
	scli_ext_cmd_handler = 0;

	// Initialize Serial Terminal
	scom_term_init(scli_term_handler);

	// Load up basic commands
	scli_def_cmd(&scli_cmd_help_s, SCLI_CMD_HELP, scli_cmd_help);
	scli_def_cmd(&scli_cmd_clear_s, SCLI_CMD_CLEAR, scli_cmd_clear);

	// Clear
	scli_clear();
}

// Initialize with Hello Message
void scli_init_m(char *hello_fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, hello_fmt);

	// Initialize
	scli_init();

	// Say Hello
	scom_term_vprintf(hello_fmt, ap);
	scli_printf("\n\n");

	// Release Args
	va_end(ap);
}

// Set Prompt
void scli_set_prompt(char *prompt)
{
	// Set Prompt
	scli_set_prompt_n(prompt, strlen(prompt));
}

// Set Prompt (fixed length)
void scli_set_prompt_n(char *prompt, uint8_t len)
{
	// Set Prompt
	scli_prompt_s = prompt;
	scli_prompt_s_len = len;
	scli_printf("\n");
	scli_prompt();
}

// Update
void scli_update()
{
	// Update Serial Terminal
	scom_term_update();
}

// Define Command
void scli_def_cmd(struct scli_cmd *c, char *cmd, void (*handler)(char **args, uint16_t *args_len))
{
	// Setup Command
	c->cmd = cmd;
	c->handler = handler;
	c->next = scli_cmd_list;

	// Insert Command into List
	scli_cmd_list = c;
}

// Set External Command Handler
void scli_set_ext_cmd_handler(scli_ext_cmd_handler_t handler, void *user)
{
	// Set External Handler
	scli_ext_cmd_handler_user = user;
	scli_ext_cmd_handler = handler;
}

// List Commands
void scli_help()
{
	struct scli_cmd *c;

	// Head
	scli_printf("Available commands:\n\n");

	// Run through Commands
	c = scli_cmd_list;
	while(c)
	{
		// Display Command & Cycle
		scli_printf(" * %s\n", c->cmd);
		c = c->next;
	}
}

// Clear Screen
void scli_clear()
{
	uint8_t i;

	// Clear
	for(i = 0; i < SCLI_CLEAR_SIZE; i = i + 1)									{ scli_printf("\n"); }
}

// Handle Serial Communication
uint8_t scli_term_handler(char *x, uint8_t s)
{
	struct scli_cmd *c;
	uint8_t cmd_l;
	char *args;
	uint16_t args_len;

	// Add Line
	scli_printf("\n");

	// Find Command
	c = scli_cmd_list;
	while(c)
	{
		// Check Command & Cycle
		if(str_is_cmd(x, s, c->cmd, &args, &args_len))
		{
			scom_term_hist_push(x, s);
			c->handler(&args, &args_len);
			goto done;
		}
		c = c->next;
	}

	// Check external handler
	if(scli_ext_cmd_handler)
	{
		// Split Command/Args
		cmd_l = 0;
		while((cmd_l < s) && (x[cmd_l] != ' '))																			{ cmd_l = cmd_l + 1; }
		args = &(x[cmd_l + 1]);
		args_len = (cmd_l < s) ? (s - (cmd_l + 1)) : 0;
		if(cmd_l)
		{
			// Run through external handler
			if(scli_ext_cmd_handler(scli_ext_cmd_handler_user, x, s, x, cmd_l, args, args_len))							{ return 0; }
		}
	}

	// Command not found
	scli_printf("Unknown Command: [%t]\n", x, s);

	// Done
	done:

	// Re-Prompt
	scli_prompt();

	return 0;
}
